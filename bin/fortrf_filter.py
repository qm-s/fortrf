#!/usr/bin/env python

"""Run F90 filters in-place"""

import argparse

from fortrf.filters import AVAILABLE_FILTERS


def main():
    args = get_argument_parser().parse_args()
    if args.list:
        list_available_filters()
    else:
        filters = parse_filters(args.filter)
        for f90file in args.f90file:
            try_run_filters(f90file, filters)


def get_argument_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-f", "--filter", type=str,
        help="comma separated filters to apply"
    )
    parser.add_argument(
        "-l", "--list", action="store_true",
        help="list avaialable filters"
    )
    parser.add_argument("f90file", type=str, nargs="*")
    return parser


def list_available_filters():
    print(" ".join(AVAILABLE_FILTERS.keys()))


def parse_filters(filters):
    return [AVAILABLE_FILTERS[key.strip()] for key in filters.split(",")]


def try_run_filters(f90file, filters):
    print("Running filters on", f90file)
    try:
        run_filters(f90file, filters)
    except (AssertionError, RuntimeError):
        print("...Failed parsing", f90file)


def run_filters(f90file, filters):
    with open(f90file) as fp:
        code = fp.read()
    cl = code.splitlines()
    for f in filters:
        cl = f(cl)
    modified = "\n".join(cl) + "\n"
    if modified != code:
        with open(f90file, "w") as fp:
            fp.write(modified)


if __name__ == "__main__":
    main()
