#!/usr/bin/env python


import subprocess

from fortrf.gccoutput import find_undefined_variables


LEADING_SPACE_FIRST_LINE = 4
LEADING_SPACE_CONTINUATION = 9
MAXIMUM_LINE_LENGTH = 78


def main():
    output = try_compile()
    variables = find_undefined_variables(output)
    integers, doubles = group_variables(variables)
    print(wrap_long_line("real(dp) :: {}".format(", ".join(doubles))))
    print(wrap_long_line("integer :: {}".format(", ".join(integers))))


def try_compile():
    return subprocess.run("make", stdout=subprocess.PIPE).stdout.decode("utf-8")


def group_variables(vars):

    def is_integer(var):
        return var[0].lower() in "ijklmn"

    integers = filter(is_integer, vars)
    doubles = filter(lambda x: not is_integer(x), vars)
    return integers, doubles


def wrap_long_line(line: str) -> str:
    chunks = line.split(", ")
    output = []
    i = 0
    maxlen = MAXIMUM_LINE_LENGTH - 2
    while i < len(chunks):
        s = start_line(i) + " " + chunks[i]
        i += 1
        if i < len(chunks):
            s += ", "
        while i < len(chunks) and len(s) + 2 + len(chunks[i]) < maxlen:
            s += chunks[i]
            i += 1
            if i < len(chunks):
                s += ", "
        if i < len(chunks):
            s += "&"
        output.append(s)
    return "\n".join(output)


def start_line(row_number: int) -> str:
    if row_number == 0:
        return " " * (LEADING_SPACE_FIRST_LINE - 1)
    else:
        return " " * LEADING_SPACE_CONTINUATION + "&"


if __name__ == "__main__":
    main()
