#!/usr/bin/env python

import argparse
from itertools import repeat
import multiprocessing
import os
import subprocess

import fortrf.f77
import fortrf.filters


def main():
    args = get_argument_parser().parse_args()
    bin_root = os.path.dirname(os.path.abspath(__file__))
    if args.serial:
        for f77 in args.f77file:
            print("Converting", f77)
            convert_file(f77, bin_root)
    with multiprocessing.Pool() as pool:
        pool.starmap(convert_file, zip(args.f77file, repeat(bin_root)))


def get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--serial", action="store_true",
        help="run F90 conversion in serial and show file name"
    )
    parser.add_argument("f77file", type=str, nargs="+")
    return parser


def convert_file(f77: str, bin_root: str):
    assert f77.lower().endswith(".f")
    lines = open(f77).read().splitlines()
    result = fortrf.f77.to_f90(lines)
    result = fortrf.filters.remove_multi_statements_per_line(result)
    result = fortrf.filters.remove_numbered_do_loops(result)
    result = fortrf.filters.replace_obsolete_intrinsic(result)
    result = fortrf.filters.replace_words(result)
    result = fortrf.filters.remove_space_in_number(result)

    # Run emacs indentation at this point to standardize format
    f90 = f77[:-1] + "F90"
    with open(f90, "w") as fp:
        fp.writelines([line + "\n" for line in result])
    fortrf_indent = os.path.join(bin_root, "fortrf_indent.py")
    subprocess.call([fortrf_indent, f90])

    lines = open(f90).read().splitlines()
    result = fortrf.filters.replace_goto_with_cycle(lines)
    result = fortrf.filters.remove_unused_lineno(result)
    result = fortrf.filters.cleanup_subprogram_titles(result)
    result = fortrf.filters.remove_return_before_end(result)
    with open(f90, "w") as fp:
        fp.writelines([line + "\n" for line in result])

    subprocess.call([fortrf_indent, f90])


if __name__ == "__main__":
    main()
