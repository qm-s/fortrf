#!/usr/bin/env python

import argparse
import os
import subprocess


def main():
    args = get_argument_parser().parse_args()
    dir = os.path.dirname(os.path.abspath(__file__))
    emacs_script = os.path.join(dir, "fortrf_indent.el")
    for f90 in args.f90file:
        assert f90.endswith(".F90")
        subprocess.call([emacs_script, f90])
        backup = f90 + "~"
        if os.path.isfile(backup):
            os.unlink(backup)


def get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("f90file", type=str, nargs="+")
    return parser


if __name__ == "__main__":
    main()
