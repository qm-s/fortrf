#!/usr/bin/env -S emacs --script

(find-file (nth 0 argv))
(f90-mode)

(setq f90-program-indent 2)
(setq f90-do-indent 2)
(setq f90-if-indent 2)
(setq f90-type-indent 2)

(indent-region (point-min) (point-max) nil)
(f90-downcase-keywords)
(delete-trailing-whitespace)
(save-buffer)
