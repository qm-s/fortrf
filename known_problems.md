In F77 the blank spaces in continued lines are stripped, so that
```
      if((ini.ne.0.and.ilauf.ne.-1).or.nii-npspac-nproj.le.0.
     >     or.cepa12) then
```
is legal.
In F90, the blank spaces in the before and after `&` will be kept.
As a result, an automatic conversion to
```
if((ini.ne.0.and.ilauf.ne.-1).or.nii-npspac-nproj.le.0. &
& or.cepa12) then
```
is illegal.
So far such cases are few and it is not worth sacrificing the visual appearance by removing the blank space before and after the ampersands.

Line number used in computed goto e.g. `3 goto (103,105,106,108,109,110,111,112),ikey` are not recognized by filter `lineno`.

Empty lines within F77 line continuation like
```
      integer i,

      ! comment
     >j
```
is not supported.
Note that the comment line abolve is also an empty line.
F77 comment lines starts with a character in the first column are allowed.
