from fortrf.filters import remove_space_in_number


def test_can_remove_spaces_in_a_number():
    code = "if(nop.gt.100 000 000) then"
    lines = code.splitlines()
    expected = code.replace("100 000 000", "100000000").splitlines()
    assert remove_space_in_number(lines) == expected
