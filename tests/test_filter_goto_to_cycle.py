from fortrf.filters import replace_goto_with_cycle


def test_can_replace_a_plain_goto_with_cycle():
    lines = """do i = 1, n
        if (i == m) goto 30
    30  continue
    end do""".splitlines()
    expected = """do i = 1, n
        if (i == m) cycle
    30  continue
    end do""".splitlines()
    assert replace_goto_with_cycle(lines) == expected


def test_can_recognize_a_do_line_with_label():
    code = """10 do i = 1, n
        if (i == m) goto 30
    30 continue
    end do"""
    lines = code.splitlines()
    expected = code.replace("goto 30", "cycle").splitlines()
    assert replace_goto_with_cycle(lines) == expected


def test_can_recognize_a_do_line_with_block_label():
    code = """  i_loop: do i = 1, n
        if (i == m) goto 30
    30 continue
    end do"""
    lines = code.splitlines()
    expected = code.replace("goto 30", "cycle").splitlines()
    assert replace_goto_with_cycle(lines) == expected


def test_can_replace_nested_goto_with_cycle():
    code = """do i = 1, n
        if (i == m) go to 30
        do j = 1, k
            if (j == l) go to 20
            20 continue
        end do
        30 continue
    enddo
    """
    lines = code.splitlines()
    expected = code.replace("go to 30", "cycle").replace("go to 20", "cycle").splitlines()
    assert replace_goto_with_cycle(lines) == expected


def test_does_not_replace_goto_outside_of_loop():
    code = """do i = 1, n
        if (i == m) go to 30
        do j = 1, k
            if (j == l) goto 30
            20 continue
        end do
        l = l + 1
        30 continue
    end do
    """
    lines = code.splitlines()
    expected = code.replace("go to 30", "cycle").splitlines()
    # Note that this does not replace "goto 30" in the inner loop
    assert replace_goto_with_cycle(lines) == expected


def test_can_replace_goto_with_break():
    code = """do i = 1, n
        if (i == m) goto 11
        end do
        11 y = y + 1
    """
    lines = code.splitlines()
    expected = code.replace("goto 11", "exit").splitlines()
    assert replace_goto_with_cycle(lines) == expected


def test_does_not_replace_goto_with_break_of_wrong_level():
    code = """do i = 1, n
        do j = 1, k
            if (j == l) goto 30
        end do
        l = l + 1
    end do
    30 continue
    """
    lines = code.splitlines()
    assert replace_goto_with_cycle(lines) == lines


def test_can_replace_with_mixed_cycle_and_exit():
    code = """do i = 1, n
        if (i == m) goto 20
        if (i == n) goto 21
        20 continue
    end do
    21 x = x + 1
    """
    lines = code.splitlines()
    expected = code.replace("goto 20", "cycle").replace("goto 21", "exit").splitlines()
    assert replace_goto_with_cycle(lines) == expected
