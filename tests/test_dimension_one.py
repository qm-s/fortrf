from fortrf.filters import replace_dimension_one_with_star


def test_can_replace_plain_dimension_one():
    code = """dimension h1(1)"""
    lines = code.splitlines()
    expected = code.replace("(1)", "(*)").splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_can_replace_dimension_one_in_continued_lines():
    code = """dimension h1(nval), &
    & h2(1)"""
    lines = code.splitlines()
    expected = code.replace("(1)", "(*)").splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_do_not_replace_dimension_one_in_assignment():
    code = """dimension h1(1)
    h1(1)=0"""
    lines = code.splitlines()
    expected = code.replace("dimension h1(1)", "dimension h1(*)").splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_do_not_replace_one_in_expression():
    code = """dimension h1(n - 1)"""
    lines = code.splitlines()
    expected = lines
    assert replace_dimension_one_with_star(lines) == expected


def test_can_replace_second_dimension():
    code = """dimension x(nval,1)"""
    lines = code.splitlines()
    expected = code.replace("1", "*").splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_do_not_repalce_array_element():
    code = """dimension x(iact(1), 1)"""
    lines = code.splitlines()
    expected = """dimension x(iact(1), *)""".splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_can_replace_multiple_instances():
    code = """  dimension d(1), &
       & cbra(1),cket(1), &
       & v1(1),v2(1), &
       & lcrs(nconr,1),lxrs(nconr,NVac), &
       & lcsi(ncons,NVac),lxsi(ncons,NVac), &
       & nspr(nconr),nsps(ncons)
    dimension icadr(1)"""
    lines = code.splitlines()
    expected = code.replace("1)", "*)").splitlines()
    assert replace_dimension_one_with_star(lines) == expected


def test_comments_are_kept_as_is():
    code = """ dimension d(1)  ! nact(1)"""
    lines = code.splitlines()
    expected = """ dimension d(*)  ! nact(1)""".splitlines()
    assert replace_dimension_one_with_star(lines) == expected
