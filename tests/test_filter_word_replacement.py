from fortrf.filters import replace_words


def test_can_apply_default_rules():
    lines = ["implicit real*8 (a-h,o-z)"]
    expected = ["implicit double precision (a-h,o-z)"]
    assert replace_words(lines) == expected


def test_can_accept_custom_rules():
    rules = {r"implicit\s+(\w+)": r"\1"}
    lines = ["implicit real*8 (a-h,o-z)"]
    expected = ["real*8 (a-h,o-z)"]
    assert replace_words(lines, rules) == expected


def test_do_not_modify_cpp_lines():
    rules = {"endif": "end if"}
    lines = ["#endif"]
    assert replace_words(lines, rules) == lines


def test_default_rules_fixes_missing_space_after_call():
    line = "if (iprint(iprdm).ge.4)calloutsqr(q(icsk),ny1,ny1,ncfs1,'CSK')"
    expected = line.replace("calloutsqr", "call outsqr")
    assert replace_words([line]) == [expected]


def test_default_rules_fixes_missing_space_after_goto():
    line = "      goto88"
    expected = line.replace("goto88", "go to 88")
    assert replace_words([line]) == [expected]


def test_default_rules_fixes_arithmetic_if():
    lines = "        if(is-js) 41,36,31".splitlines()
    expected = """        if ((is-js) < 0) then
go to 41
else if ((is-js) == 0) then
go to 36
else
go to 31
end if
""".splitlines()
    assert replace_words(lines) == expected
