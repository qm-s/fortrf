from fortrf.filters import replace_obsolete_intrinsic


def test_can_replace_obsolete_intrinsic():
    lines = ["Eps1c=Dble( Mod(IAbs(ishifte0),1000) )"]
    expected = ["Eps1c=Dble( Mod(abs(ishifte0),1000) )"]
    assert replace_obsolete_intrinsic(lines) == expected
