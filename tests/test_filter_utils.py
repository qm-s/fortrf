from fortrf.filters.utils import run_line_filter


def test_line_filter_runs_function():
    code = """#define x
    line ! comment
    line"""
    lines = code.splitlines()
    expected = code.replace("line", "X").splitlines()
    assert run_line_filter(lines, lambda x: x.replace("line", "X")) == expected
