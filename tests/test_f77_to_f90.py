from fortrf.f77 import to_f90


def test_convert_mixed_lines():
    lines = """#ifndef oldmatinv
c          de=0
           call ludcmp(q(itrm))
#endif
""".splitlines()
    expected = """#ifndef oldmatinv
! de=0
           call ludcmp(q(itrm))
#endif
""".splitlines()
    assert to_f90(lines) == expected


def test_can_convert_continued_lines():
    lines = """          call outsqr(tr(ioff+noc*nt(isk)+1),
     >       nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    expected = """          call outsqr(tr(ioff+noc*nt(isk)+1), &
& nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    assert to_f90(lines) == expected


def test_do_not_add_ampersand_to_comment_line():
    lines = """          call outsqr(tr(ioff+noc*nt(isk)+1),
!            some inserted comment
     >       nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    expected = """          call outsqr(tr(ioff+noc*nt(isk)+1), &
! some inserted comment
& nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    assert to_f90(lines) == expected


def test_can_add_ampersand_to_line_with_comment():
    lines = """          call outsqr(tr(ioff+noc*nt(isk)+1),  ! LINE-ONE
     >       nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    expected = """          call outsqr(tr(ioff+noc*nt(isk)+1), &  ! LINE-ONE
& nt(isk),nt(isk),nse,'VE fu')
""".splitlines()
    assert to_f90(lines) == expected
