import pytest

from fortrf.filters.numbered_do import *
from fortrf.f90 import f90_lines_from_code_lines


def test_can_discover_numbered_do():
    code = """do 10 i=1,n
    a(i)=i
    10 continue""".splitlines()
    lines = f90_lines_from_code_lines(code)
    expected_do = {0}
    expected_insert = {2: 1}
    assert find_numbered_do(lines) == (expected_do, expected_insert)


def test_can_discover_nested_numbered_do():
    code = """do 10 i=1,n
    do 10 j=1,n
    a(i,j)=i*j
    10 continue""".splitlines()
    lines = f90_lines_from_code_lines(code)
    expected_do = {0, 1}
    expected_insert = {3: 2}
    assert find_numbered_do(lines) == (expected_do, expected_insert)


def test_can_recognize_comma_after_line_label():
    code = """do 10,i=1,n
    a(i)=i
    10 continue""".splitlines()
    lines = f90_lines_from_code_lines(code)
    expected_do = {0}
    expected_insert = {2: 1}
    assert find_numbered_do(lines) == (expected_do, expected_insert)


def test_raises_an_error_when_numbered_do_is_not_matched():
    code = """do 10 i=1,n
    a(i)=i
    end do""".splitlines()
    lines = f90_lines_from_code_lines(code)
    with pytest.raises(RuntimeError):
        _ = find_numbered_do(lines)


def test_can_refactor_single_numbered_do_loop():
    lines = """      do 870 istsym=1,nstsym
      if (istms2(istsym).ne.ms2) call error('illegal ms2','cicon')
870   nconmx = nconmx + istncn(istsym)
""".splitlines()
    expected = """      do istsym=1,nstsym
      if (istms2(istsym).ne.ms2) call error('illegal ms2','cicon')
870   nconmx = nconmx + istncn(istsym)
end do
""".splitlines()
    assert remove_numbered_do_loops(lines) == expected


def test_do_not_modify_commented_numbered_do_loop():
    lines = """! do 10 i = 1, 10
! 10 x = x + i
""".splitlines()
    assert remove_numbered_do_loops(lines) == lines


def test_can_refactor_nested_numbered_do_loops():
    lines = """do 10 i = 1, n
do 10 j = 1, n
do 20 k = 1, n
20 x = x + k
10 continue
""".splitlines()
    expected = """do i = 1, n
do j = 1, n
do k = 1, n
20 x = x + k
end do
10 continue
end do
end do
""".splitlines()
    assert remove_numbered_do_loops(lines) == expected


def test_do_not_modify_do_loop_in_string_literal():
    lines = """print *, 'do 10 i = 1, n'
10 continue
""".splitlines()
    assert remove_numbered_do_loops(lines) == lines


def test_can_remove_comma_after_line_label():
    lines = """do 10,i=1,n
10 continue""".splitlines()
    expected = """do i=1,n
10 continue
end do""".splitlines()
    assert remove_numbered_do_loops(lines) == expected
