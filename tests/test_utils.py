from fortrf.utils import *


def test_findall_returns_empty_iterator_when_not_found():
    assert list(findall("y", "x")) == []


def test_findall_finds_all_instances():
    s = "xx+x"
    expected = [0, 1, 3]
    assert list(findall("x", s)) == expected
