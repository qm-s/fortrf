import pytest

from fortrf.f90line import F90Line


def test_a_simple_code_line_is_not_masked():
    code = "x = x + 1"
    line = F90Line(code)
    assert line.lines == [code]
    assert line.masked == code
    assert line.masks == {}


def test_leading_space_is_stripped_from_masked_line():
    code = "  x = x + 1"
    line = F90Line(code)
    assert line.lines == [code]
    assert line.masked == code.lstrip()
    assert line.masks == {}


def test_can_reproduce_simple_code_line_with_leading_spaces():
    code = "  x = x + 1"
    line = F90Line(code)
    assert line.get_code() == code


def test_an_empty_line_is_represented_and_can_be_restored():
    code = ""
    line = F90Line(code)
    assert line.masked == ""
    assert line.get_code() == code


def test_cpp_line_is_masked_in_full():
    code = "#ifndef oldmatinv"
    line = F90Line(code)
    assert "#" not in line.masked
    assert "ifndef" not in line.masked
    assert "oldmatinv" not in line.masked
    assert line.get_code() == code


def test_raises_an_error_on_indented_cpp_line():
    code = "  #endif"
    with pytest.raises(RuntimeError):
        line = F90Line(code)


def test_can_mask_string_literals():
    code = "x='err'"
    line = F90Line(code)
    expected_masked = "x= _S1_ "
    expected_masks = {"S1": "'err'"}
    assert line.masked == expected_masked
    assert line.masks == expected_masks


def test_can_restore_masked_string_literals():
    code = "x='err'"
    line = F90Line(code)
    assert "err" not in line.masked
    assert line.get_code() == code


def test_can_mask_and_restore_line_continuation():
    code = """  x = x + 1 &
        & + y"""
    line = F90Line(code)
    assert "&" not in line.masked
    assert "y" in line.masked
    assert line.get_code() == code


def test_can_mask_multiple_line_continuations_with_comments():
    code = """  x = x // "1&" &
        & // y &  ! Inline comment
        ! Single line comment
        & // z  ! EOL comment"""
    line = F90Line(code)
    assert "&" not in line.masked
    assert "comment" not in line.masked
    assert "// y" in line.masked
    assert "// z" in line.masked
    assert line.get_code() == code


def test_ignore_ampersands_in_comment():
    code = """  x = x // "1&" &
        & // y &  ! Inline & comment
        ! Single & line & comment
        & // z  ! EOL comment"""
    line = F90Line(code)
    assert "&" not in line.masked
    assert "comment" not in line.masked
    assert line.get_code() == code


def test_can_mask_and_restore_multiple_string_literals():
    code = """  x = 'str1' // "str2" // x"""
    line = F90Line(code)
    assert "str" not in line.masked
    assert line.get_code() == code


def test_can_mask_and_restore_string_literals_and_comment():
    code = """  x=x//"err" ! comment"""
    line = F90Line(code)
    assert "err" not in line.masked
    assert "!" not in line.masked
    assert "comment" not in line.masked
    assert line.get_code() == code


def test_comment_with_string_literal_is_reproduced_as_is():
    code = """ x = 1 ! print *, 'this should not get lost'"""
    line = F90Line(code)
    assert line.get_code() == code


def test_can_remove_line_label():
    code = " 100 x = 0"
    expected = "     x = 0"
    line = F90Line(code)
    line.remove_line_label()
    assert line.masked == "x = 0"
    assert line.get_code() == expected


def test_error_when_trying_to_remove_nonexist_line_label():
    code = "  x = 0"
    line = F90Line(code)
    with pytest.raises(RuntimeError):
        line.remove_line_label()


def test_can_recognize_a_blank_line():
    line = F90Line("")
    assert line.is_blank()


def test_a_line_with_comment_is_not_blank():
    line = F90Line("!")
    assert not line.is_blank()


def test_a_blank_line_is_meaningless():
    line = F90Line("")
    assert line.is_meaningless()


def test_a_separator_line_is_meaningless():
    line = F90Line("! --------------------")
    assert line.is_meaningless()


def test_a_line_with_comment_is_not_meaningless():
    line = F90Line("  ! Comment")
    assert not line.is_meaningless()


def test_can_set_a_line_blank():
    line = F90Line("x=x+1")
    line.set_blank()
    assert line.is_blank()
    assert line.get_code() == ""


def test_trailing_spaces_are_removed_after_setting_blank():
    line = F90Line("  x=x+1")
    line.set_blank()
    assert line.get_code() == ""


def test_get_code_returns_none_on_deleted_line():
    line = F90Line("x=x+1")
    line.set_deleted()
    assert line.deleted
    assert line.get_code() is None
