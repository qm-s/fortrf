from fortrf.f90 import *


def test_can_strip_string_literals():
    line = "write (iout,*) ' do 1100  ',ponsym,iconn"
    expected = "write (iout,*) ,ponsym,iconn"
    assert strip_literals(line) == expected


def test_can_strip_comments():
    line = "do 1100 i = 1, n  ! comment"
    expected = "do 1100 i = 1, n  "
    assert strip_literals(line) == expected


def test_can_strip_mixed_string_literals_and_comments():
    line = "print *, 'do 1100!'  ! comment"
    expected = "print *,   "
    assert strip_literals(line) == expected


def test_f90_lines_from_code_lines_can_handle_empty_list():
    assert f90_lines_from_code_lines([]) == []


def test_can_create_a_list_of_line_objects():
    code = ["do i = 1, n", "  x = 1 + i", "end do"]
    lines = f90_lines_from_code_lines(code)
    for i, c in enumerate(code):
        assert c == lines[i].get_code()


def test_can_combine_continued_lines():
    code = """x = 1 &\n & + y""".splitlines()
    lines = f90_lines_from_code_lines(code)
    assert len(lines) == 1
    assert "\n".join(code) == lines[0].get_code()


def test_can_combine_continued_lines_with_comment_line_in_between():
    code = """x = 1 & ! inline-comment
    ! standalone comment
    & + y""".splitlines()
    lines = f90_lines_from_code_lines(code)
    assert(len(lines) == 1)
    assert "comment" not in lines[0].masked
    assert "\n".join(code) == lines[0].get_code()


def test_do_not_combine_extra_comment_lines():
    code = """x = 1
    ! comment
    ! extra comment""".splitlines()
    lines = f90_lines_from_code_lines(code)
    assert(len(lines) == 3)
    assert all("comment" not in line.masked for line in lines)
    assert code_lines_from_f90_lines(lines) == code


def test_can_backconvert_f90_lines_to_code_lines():
    code = """x = 1  ! comment\n ! extra comment""".splitlines()
    lines = f90_lines_from_code_lines(code)
    assert code_lines_from_f90_lines(lines) == code


def test_can_backconvert_f90_lines_with_blank_lines():
    code = "x\n\ny".splitlines()
    lines = f90_lines_from_code_lines(code)
    assert code_lines_from_f90_lines(lines) == code


def test_deleted_lines_are_deleted_after_conversion():
    code = """x = 1
    y = 1""".splitlines()
    lines = f90_lines_from_code_lines(code)
    lines[0].set_deleted()
    assert code_lines_from_f90_lines(lines) == code[1:]
