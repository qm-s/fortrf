from fortrf.filters import remove_multi_statements_per_line


def test_can_remove_multi_statements_per_line():
    code = "  do i=1,n; a(i)=i; end do"
    lines = code.splitlines()
    expected = code.replace(";", "\n").splitlines()
    assert remove_multi_statements_per_line(lines) == expected
