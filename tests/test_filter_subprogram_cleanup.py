from fortrf.filters import cleanup_subprogram_titles


def test_can_add_two_blank_lines_before_subroutines():
    code = """subroutine x
    end subroutine x
    subroutine y
    end subroutine y"""
    lines = code.splitlines()
    expected = lines[:2] + ["", ""] + lines[2:]
    assert cleanup_subprogram_titles(lines) == expected


def test_do_not_add_blank_lines_if_already_exist():
    code = """subroutine x
    end subroutine x


    subroutine y
    end subroutine y"""
    lines = code.splitlines()
    assert cleanup_subprogram_titles(lines) == lines


def test_insert_only_one_blank_line_if_one_already_exists():
    code = """subroutine x
    end subroutine x

    subroutine y
    end subroutine y"""
    lines = code.splitlines()
    expected = lines[:2] + [""] + lines[2:]
    assert cleanup_subprogram_titles(lines) == expected


def test_can_set_meaningless_lines_blank_before_subprogram():
    code = """subroutine x
    end subroutine x
    !
    ! ----------
    subroutine y
    end subroutine y"""
    lines = code.splitlines()
    expected = lines[:2] + ["", ""] + lines[4:]
    assert cleanup_subprogram_titles(lines) == expected


def test_do_not_produce_blank_lines_at_the_beginning_of_file():
    code = """! -------------
    subroutine x
    end subroutine x"""
    lines = code.splitlines()
    expected = lines[1:]
    assert cleanup_subprogram_titles(lines) == expected


def test_functions_are_also_subprograms():
    code = """!
    function x()
    end function x
    real(dp) function y()
    end function y"""
    lines = code.splitlines()
    expected = lines[1: 3] + ["", ""] + lines[3:]
    assert cleanup_subprogram_titles(lines) == expected
