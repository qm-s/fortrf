from fortrf.f77 import *


def test_can_identify_cpp_line():
    line = "#ifndef oldmatinv"
    assert is_line_cpp(line)


def test_comment_line_is_not_cpp():
    line = "c... core contribution"
    assert not is_line_cpp(line)


def test_can_identify_comment_line():
    line = "c... Mix Active and Secondary"
    assert is_line_comment(line)


def test_cpp_line_is_not_comment():
    line = "#ifndef oldmatinv"
    assert not is_line_comment(line)


def test_line_starting_with_number_is_not_comment():
    line = "1         format(a,' Sym ',i2)"
    assert not is_line_comment(line)


def test_regular_code_line_is_not_comment():
    line = "           itt=icorr(nva)"
    assert not is_line_comment(line)


def test_can_identify_continuation_line():
    line = "     >          (i.gt.ncl.and.j.le.ncl)) call error("
    assert is_line_continuation(line)


def test_first_line_is_not_continuation():
    line = "           do jj=0,nva"
    assert not is_line_continuation(line)


def test_can_split_inline_comment():
    line = "      ioff=0   ! inline comment"
    code = "      ioff=0   "
    comment = "! inline comment"
    assert split_comment(line) == (code, comment)


def test_exclamation_mark_in_string_literal_does_not_start_comment():
    line = """      ioff="ain't comment!"  ! comment"""
    code = """      ioff="ain't comment!"  """
    comment = "! comment"
    assert split_comment(line) == (code, comment)


def test_split_comment_can_handle_line_without_comment():
    line = "           do jj=0,nva"
    assert split_comment(line) == (line, "")


def test_comment_line_is_not_continuation():
    line = "! comment"
    assert not is_line_continuation(line)


def test_can_convert_comment_line():
    line = "c       expression analyzer"
    expected = "! expression analyzer"
    assert comment_to_f90(line) == expected


def test_can_convert_continuation_line():
    line = "     >          (i.gt.ncl.and.j.le.ncl)) call error("
    expected = "& (i.gt.ncl.and.j.le.ncl)) call error("
    assert continuation_to_f90(line) == expected
