from fortrf.filters import remove_return_before_end


def test_can_remove_return_before_end_subroutine():
    code = """subroutine x
    return
    end subroutine x"""
    lines = code.splitlines()
    expected = [lines[0], lines[2]]
    assert remove_return_before_end(lines) == expected


def test_can_remove_return_before_end_function():
    code = """type(y) function x()
    return
    end function x"""
    lines = code.splitlines()
    expected = [lines[0], lines[2]]
    assert remove_return_before_end(lines) == expected


def test_do_not_remove_return_not_at_the_end():
    code = """subroutine x
    if (y) then
        return
    end if
    end subroutine x"""
    lines = code.splitlines()
    assert remove_return_before_end(lines) == lines


def test_do_not_remove_return_with_comment():
    code = """subroutine x
    return  ! we are done
    end subroutine x"""
    lines = code.splitlines()
    assert remove_return_before_end(lines) == lines


def test_do_not_remove_return_with_line_number():
    code = """subroutine x
    100 return
    end subroutine x"""
    lines = code.splitlines()
    assert remove_return_before_end(lines) == lines
