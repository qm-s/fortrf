from fortrf.navitagion import *


def read_file(filename):
    return open(filename).readlines()


def test_can_locate_simple_subroutine_call():
    lines = read_file("tests/data/cif12.F90")
    linenos = list(locate_subroutine_calls(lines, "corlsr"))
    assert len(linenos) == 1
    assert "call corlsr(ibase)" in lines[linenos[0]]


def test_can_locate_multiple_calls_of_different_styles():
    lines = read_file("tests/data/cif12.F90")
    linenos = list(locate_subroutine_calls(lines, "cidcp"))
    assert len(linenos) == 2
    assert "call CiDcp (iter,dum,idum);" in lines[linenos[0]]
    assert "call cidcp(iter,dum,idum)" in lines[linenos[1]]
