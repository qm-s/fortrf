from fortrf.gccoutput import *


def test_can_detect_undefined_variable():
    msg = """../src/mrci/cipairmp2.F90:172:12:

  172 |         do i=1,ns
      |            1
Error: Symbol ‘i’ at (1) has no IMPLICIT type; did you mean ‘iq’?
"""
    expected = ["i"]
    assert find_undefined_variables(msg) == expected


def test_can_detect_multiple_variables():
    msg = """../src/mrci/cipairmp2.F90:168:6:

  168 |     ia=icref-1
      |      1
Error: Symbol ‘ia’ at (1) has no IMPLICIT type; did you mean ‘iq’?
../src/mrci/cipairmp2.F90:169:10:

  169 |     do  ic=ncnof(isyref)+1,ncnof(isyref)+ncosym(isyref)
      |          1
Error: Symbol ‘ic’ at (1) has no IMPLICIT type; did you mean ‘iq’?
"""
    expected = ["ia", "ic"]
    assert find_undefined_variables(msg) == expected
