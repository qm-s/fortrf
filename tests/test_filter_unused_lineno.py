import pytest

from fortrf.filters import remove_unused_lineno
from fortrf.filters.unused_lineno import _LineNo


def test_new_lineno_has_no_unused():
    ln = _LineNo()
    assert ln.get_unused() == {}


def test_lineno_accepts_registers():
    ln = _LineNo()
    ln.register("100", 1)
    ln.register("200", 2)
    assert ln.get_unused() == {100: 1, 200: 2}


def test_lineno_refuses_multiple_registers():
    ln = _LineNo()
    ln.register("100", 1)
    with pytest.raises(RuntimeError):
        ln.register("100", 2)


def test_used_lineno_are_not_reported():
    ln = _LineNo()
    ln.register("100", 1)
    ln.register("200", 2)
    ln.use("200")
    assert ln.get_unused() == {100: 1}


def test_can_remove_single_line_label():
    code = ["10  x = x + 1"]
    expected = ["    x = x + 1"]
    assert remove_unused_lineno(code) == expected


def test_do_not_remove_used_line_label():
    code = """10 x = x + 1
    if (ok) go to 10
    """.splitlines()
    assert remove_unused_lineno(code) == code


def test_do_not_remove_used_format_label():
    code = """write(iout1 , 11) x
    11 format(i1)
    """.splitlines()
    assert remove_unused_lineno(code) == code


def test_can_remove_line_label_used_in_another_subroutine():
    code = """subroutine x(a)
        10 x = x + 1
    end subroutine x
    subroutine y(a)
        go to 10
        10 continue
    end subroutine y
    """
    lines = code.splitlines()
    expected = code.replace("10 x", "   x").splitlines()
    assert remove_unused_lineno(lines) == expected


def test_can_remove_line_label_in_the_last_subroutine():
    code = """    subroutine y(a)
        go to 10
        10 continue
    end subroutine y
    subroutine x(a)
        10 x = x + 1
    end subroutine x
    """
    lines = code.splitlines()
    expected = code.replace("10 x", "   x").splitlines()
    assert remove_unused_lineno(lines) == expected


def test_can_remove_line_label_used_in_another_plain_function():
    code = """subroutine x(a)
        10 x = x + 1
    end subroutine x
    function y(a)
        go to 10
        10 continue
    end function y
    """
    lines = code.splitlines()
    expected = code.replace("10 x", "   x").splitlines()
    assert remove_unused_lineno(lines) == expected


def test_can_remove_line_label_used_in_another_typed_function():
    code = """subroutine x(a)
        10 x = x + 1
    end subroutine x
    real(dp) function y(a)
        go to 10
        10 continue
    end function y
    """
    lines = code.splitlines()
    expected = code.replace("10 x", "   x").splitlines()
    assert remove_unused_lineno(lines) == expected


def test_can_remove_unused_continue_line():
    code = """do i = 1, n
    x = x + 1
    10 continue
    end do
    """
    lines = code.splitlines()
    expected = lines[:2] + lines[3:]
    assert remove_unused_lineno(lines) == expected


def test_do_not_remove_continue_line_with_comment():
    code = """do i = 1, n
    x = x + 1
    10 continue  ! end i loop
    end do
    """
    lines = code.splitlines()
    expected = code.replace("10", "  ").splitlines()
    assert remove_unused_lineno(lines) == expected


def test_can_remove_unused_multiline_format():
    lines = """34 format(1x, &
    & i1)
    x=x+1""".splitlines()
    assert remove_unused_lineno(lines) == lines[-1:]


def test_can_recognize_line_with_both_label_and_goto():
    code = """10 x = x + 1
    20 if(y) goto 10
    """
    lines = code.splitlines()
    expected = code.replace("20", "  ").splitlines()
    assert remove_unused_lineno(lines) == expected
