import re
from typing import Optional


class F90Line:

    def __init__(self, code: str):
        self.lines = [code]
        self.masked = code.lstrip()
        self.leading_spaces = len(code) - len(self.masked)
        self.masks = {}
        self.masked = self._mask_cpp(self.masked)
        self.masked = self._mask_string_literal(self.masked)
        self.masked = self._mask_line_continuation(self.masked)
        self.masked = self._mask_comment(self.masked)
        self.deleted = False

    def get_code(self) -> Optional[str]:
        if self.deleted:
            return None
        s = self.masked
        for key, val in self.masks.items():
            s = s.replace(f" _{key}_ ", val)
        return " " * self.leading_spaces + s

    def _mask_cpp(self, code: str) -> str:
        if code and code[0] == "#":
            if self.leading_spaces:
                raise RuntimeError("Unexpected cpp line")
            self.masks["CPP"] = code
            return " _CPP_ "
        else:
            return code

    def _mask_string_literal(self, code: str) -> str:
        result = ""
        literal = ""
        n_literal = 0
        comment = False
        for c in code:
            if comment:
                result += c
                if c == "\n":
                    comment = False
            elif literal:
                literal += c
                if c == literal[0]:
                    n_literal += 1
                    result += f" _S{n_literal}_ "
                    self.masks[f"S{n_literal}"] = literal
                    literal = ""
            else:
                if c in "\'\"":
                    literal = c
                else:
                    result += c
                    if c == "!":
                        comment = True
        return result

    def _mask_line_continuation(self, code: str) -> str:
        # Remove line continuation, provided that there are not string literals

        def findall_lc_ampersand(code):
            result = []
            comment = False
            for i, c in enumerate(code):
                if comment:
                    if c == "\n":
                        comment = False
                else:
                    if c == "!":
                        comment = True
                    elif c == "&":
                        result.append(i)
            return result

        pos = list(findall_lc_ampersand(code))
        if not pos:
            return code
        assert len(pos) % 2 == 0
        result = ""
        begin = 0
        n_lc = 0
        for abegin, aend in zip(pos[::2], pos[1::2]):
            result += code[begin: abegin]
            n_lc = n_lc + 1
            self.masks[f"L{n_lc}"] = code[abegin: aend + 1]
            result += f" _L{n_lc}_ "
            begin = aend + 1
        result += code[begin:]
        return result

    def _mask_comment(self, code: str) -> str:
        # Remove comments, provided that there are no string linerals
        idx = code.find("!")
        if idx == -1:
            return code
        self.masks["C1"] = code[idx:]
        return code[:idx] + " _C1_ "

    def remove_line_label(self):
        label = self.masked.split()[0]
        try:
            _ = int(label)
        except ValueError:
            raise RuntimeError("Line does not contain a label")
        n = len(self.masked)
        self.masked = self.masked[len(label):].lstrip()
        self.leading_spaces += n - len(self.masked)

    def is_blank(self):
        return self.masked == ""

    def is_meaningless(self):
        return not re.search(r"\w", self.get_code())

    def set_blank(self):
        self.masked = ""
        self.masks = {}
        self.leading_spaces = 0

    def set_deleted(self):
        self.set_blank()
        self.deleted = True
