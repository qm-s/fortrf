from collections import defaultdict
import re

from fortrf.f90line import F90Line
from ..f90 import f90_lines_from_code_lines, code_lines_from_f90_lines


def remove_numbered_do_loops(code: list[str]) -> list[str]:
    re_do = re.compile(r"(do\s+\d+\W+)\w")

    def remove_number_in_do_statement(line: F90Line):
        match = re_do.search(line.masked)
        assert match
        line.masked = line.masked.replace(match.group(1), "do ")

    lines = f90_lines_from_code_lines(code)
    do_lines, add_after = find_numbered_do(lines)
    result = []
    for i, line in enumerate(lines):
        if i in do_lines:
            remove_number_in_do_statement(line)
        result.append(line)
        if i in add_after:
            for _ in range(add_after[i]):
                result.append(F90Line("end do"))
    return code_lines_from_f90_lines(result)


def find_numbered_do(
    lines: list[F90Line]
) -> tuple[set[int], dict[int, int]]:

    re_do = re.compile(r"do\s+(\d+)\W+\w")
    re_lineno = re.compile(r"^(\d+)\b")

    def find_matching_numbered_do(
        lines: list[F90Line], begin: int, lineno: str
    ) -> int:
        for i, line in enumerate(lines[begin:], begin):
            if match := re_lineno.match(line.masked):
                if match.group(1) == lineno:
                    return i
        raise RuntimeError(f"Cannot find line number {lineno}")

    do_lines = set()
    add_after = defaultdict(int)
    for i, line in enumerate(lines):
        if match := re_do.search(line.masked):
            do_lines.add(i)
            j = find_matching_numbered_do(lines, i + 1, match.group(1))
            add_after[j] += 1
    return do_lines, add_after
