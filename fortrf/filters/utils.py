from typing import Callable

from ..f90 import f90_lines_from_code_lines, code_lines_from_f90_lines


def run_line_filter(code: list[str], fn: Callable[[str], str]) -> list[str]:
    lines = f90_lines_from_code_lines(code)
    for line in lines:
        line.masked = fn(line.masked)
    return code_lines_from_f90_lines(lines)
