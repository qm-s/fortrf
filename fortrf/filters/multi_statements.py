"""Remove multiple statements per line

Such constructs confuse emacs when indenting the code and cause
may additional difficulties when refactoring the code.

"""


from .utils import run_line_filter


def remove_multi_statements_per_line(code: list[str]) -> list[str]:
    return run_line_filter(code, lambda x: x.replace(";", "\n"))
