import re

from .utils import run_line_filter


def remove_space_in_number(lines: list[str]) -> list[str]:
    re_number = re.compile(r"\b\d+\s[0-9 ]+\d\b")

    def patch_line(line: str) -> str:
        if match := re_number.search(line):
            num = match.group(0)
            if " " in num:
                line = line.replace(num, num.replace(" ", ""))
        return line

    return run_line_filter(lines, patch_line)
