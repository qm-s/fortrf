import re

from .utils import f90_lines_from_code_lines, code_lines_from_f90_lines
from ..f90line import F90Line


def cleanup_subprogram_titles(code: list[str]) -> list[str]:

    re_subroutine = re.compile(r"^subroutine\s+\w", re.IGNORECASE)
    re_function = re.compile(r"\bfunction\s+\w", re.IGNORECASE)

    def is_function_start(line: F90Line):
        if line.masked.lower().startswith("end"):
            # `end function` with match the regex as well
            return False
        return re_function.search(line.masked)

    def is_subprogram_start(line: F90Line):
        return re_subroutine.search(line.masked) or is_function_start(line)

    lines = f90_lines_from_code_lines(code)
    result = []
    for i, line in enumerate(lines):
        if is_subprogram_start(line):
            set_meaningless_lines_blank(lines, i)
            result.extend([F90Line("")] * n_blank_line_required(lines, i))
        result.append(line)

    delete_leading_blank_lines(result)
    result = code_lines_from_f90_lines(result)
    return result


def set_meaningless_lines_blank(lines: list[F90Line], subp: int):
    """Set up to 2 meaningless lines before `lines[subp]` blank"""
    if subp > 0 and lines[subp - 1].is_meaningless():
        lines[subp - 1].set_blank()
        if subp > 1 and lines[subp - 2].is_meaningless():
            lines[subp - 2].set_blank()


def n_blank_line_required(lines: list[F90Line], subp: int) -> int:
    """Return the number of blank lines to be inserted before `lines[subp]`"""
    if subp == 0:
        return 0
    n_blank = 0
    if subp > 0 and lines[subp - 1].is_blank():
        n_blank += 1
        if subp > 1 and lines[subp - 2].is_blank():
            n_blank += 2
    return 2 - n_blank


def delete_leading_blank_lines(lines: list[F90Line]):
    for line in lines:
        if line.is_blank():
            line.set_deleted()
        else:
            return
