import re
from functools import partial

from .utils import run_line_filter


REPLACES = {
    "dabs": "abs",
    "iabs": "abs",
    "dexp": "exp",
    "dlog": "log",
    "dmin1": "min",
    "min0": "min",
    "dmax1": "max",
    "max0": "max",
    "dsign": "sign",
    "isign": "sign",
    "dsqrt": "sqrt",
}


def replace_obsolete_intrinsic(lines: list[str]) -> list[str]:
    all_res = compile_all_regex()
    return run_line_filter(
        lines,
        partial(replace_obsolete_intrinsic_in_line, all_res=all_res)
    )


def replace_obsolete_intrinsic_in_line(line: str, all_res) -> str:
    for pattern, replace in all_res:
        line = pattern.sub(replace, line)
    return line


def compile_all_regex() -> list[tuple[re.Pattern, str]]:

    def compile_regex(func):
        regex = rf"\b{func}(\s*\()"
        return re.compile(regex, re.IGNORECASE)

    def replace_string(func):
        return func + r"\1"

    return [
        (compile_regex(original), replace_string(replaced))
        for original, replaced in REPLACES.items()
    ]
