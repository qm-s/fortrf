import re

from ..f90 import f90_lines_from_code_lines, code_lines_from_f90_lines


def replace_goto_with_cycle(code: list[str]) -> list[str]:
    re_do = re.compile(r"^\d*\s*(?:\w+\s*:)?\s*do\b", re.IGNORECASE)
    re_enddo = re.compile(r"\bend\s*do", re.IGNORECASE)
    re_goto = re.compile(r"\bgo\s*to\s+(\d+)\b", re.IGNORECASE)
    re_continue = re.compile(r"^(\d+)\s*continue$", re.IGNORECASE)
    re_lineno = re.compile(r"^(\d+)\s")

    def process_loop(lines, enddo, goto):
        pre_enddo = lines[enddo - 1].masked.strip()
        if match_cycle := re_continue.fullmatch(pre_enddo):
            for i, match_goto in goto.items():
                if match_goto.group(1) == match_cycle.group(1):
                    lines[i].masked = lines[i].masked.replace(
                        match_goto.group(0), "cycle"
                    )
        if len(lines) > enddo + 1:
            after_enddo = lines[enddo + 1].masked.strip()
            if match_exit := re_lineno.match(after_enddo):
                for i, match_goto in goto.items():
                    if match_goto.group(1) == match_exit.group(1):
                        lines[i].masked = lines[i].masked.replace(
                            match_goto.group(0), "exit"
                        )

    lines = f90_lines_from_code_lines(code)

    gotos = []
    # One {code line with go to: match obj} for each do ... end do level
    for i, line in enumerate(lines):
        s = line.masked.strip()
        if re_do.search(s):
            gotos.append({})
        elif match_goto := re_goto.search(s):
            if gotos:
                gotos[-1][i] = match_goto
        elif re_enddo.search(s):
            process_loop(lines, i, gotos.pop())
    assert not gotos

    return code_lines_from_f90_lines(lines)
