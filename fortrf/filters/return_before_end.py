import re

from ..f90 import f90_lines_from_code_lines, code_lines_from_f90_lines
from ..f90line import F90Line


def remove_return_before_end(code: list[str]) -> list[str]:
    re_end_subp = re.compile(r"^end (?:subroutine|function)\b", re.IGNORECASE)

    lines = f90_lines_from_code_lines(code)
    for i, line in enumerate(lines):
        if i > 0 and re_end_subp.match(line.masked):
            if lines[i - 1].masked.lower() == "return":
                lines[i - 1].set_deleted()

    return code_lines_from_f90_lines(lines)
