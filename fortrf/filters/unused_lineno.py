import re

from fortrf.f90line import F90Line

from .. import f90


class _LineNo:

    def __init__(self):
        self.linenos = {}
        self.used = set()

    def get_unused(self):
        return {label: line for label, line in self.linenos.items()
                if label not in self.used}

    def register(self, label, code_line):
        label = int(label)
        if label in self.linenos:
            raise RuntimeError(f"Line label {label} already seen")
        self.linenos[int(label)] = code_line

    def use(self, label):
        self.used.add(int(label))


def remove_unused_lineno(code: list[str]) -> list[str]:
    """
    Remove unused line numbers

    """

    re_lineno = re.compile(r"^(\d+)\s")
    re_goto = re.compile(r"\bgo\s*to\s+(\d+)\b", re.IGNORECASE)
    re_write = re.compile(r"\bwrite\s*\(\s*\w+\s*,\s*(\d+)\s*\)",
                          re.IGNORECASE)
    re_subroutine = re.compile(r"^subroutine\s", re.IGNORECASE)
    re_function = re.compile(r"\bfunction\s", re.IGNORECASE)
    re_format = re.compile(r"^format\s*\(", re.IGNORECASE)

    def apply_changes(lines: list[F90Line], ln: _LineNo):
        for _, i in ln.get_unused().items():
            lines[i].remove_line_label()
            if lines[i].masked.strip().lower() == "continue" \
                    or re_format.match(lines[i].masked):
                lines[i].set_deleted()

    lines = f90.f90_lines_from_code_lines(code)
    ln = _LineNo()
    for i, line in enumerate(lines):
        s = line.masked
        if match := re_lineno.match(s):
            ln.register(match.group(1), i)
        if match := re_goto.search(s):
            ln.use(match.group(1))
        if match := re_write.search(s):
            ln.use(match.group(1))
        if re_subroutine.match(s) or re_function.search(s):
            apply_changes(lines, ln)
            ln = _LineNo()
    apply_changes(lines, ln)

    return f90.code_lines_from_f90_lines(lines)
