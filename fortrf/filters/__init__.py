from .numbered_do import remove_numbered_do_loops
from .obsolete_intrinsic import replace_obsolete_intrinsic
from .word_replacement import replace_words
from .space_in_number import remove_space_in_number
from .goto_to_cycle import replace_goto_with_cycle
from .unused_lineno import remove_unused_lineno
from .subprogram_cleanup import cleanup_subprogram_titles
from .return_before_end import remove_return_before_end
from .dimension_one import replace_dimension_one_with_star
from .multi_statements import remove_multi_statements_per_line


AVAILABLE_FILTERS = {
    "doloop": remove_numbered_do_loops,
    "intrinsic": replace_obsolete_intrinsic,
    "word": replace_words,
    "goto": replace_goto_with_cycle,
    "lineno": remove_unused_lineno,
    "spacenum": remove_space_in_number,
    "subp": cleanup_subprogram_titles,
    "return": remove_return_before_end,
    "dim1": replace_dimension_one_with_star,
    "mstat": remove_multi_statements_per_line,
}
