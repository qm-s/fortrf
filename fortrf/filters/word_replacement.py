from functools import partial
import re

from .utils import run_line_filter


REGEX_ARITHMETIC_IF = r"^if\s*\((.*)\)\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*$"
REPLACE_ARITHMETIC_IF = r"""if ((\1) < 0) then
go to \2
else if ((\1) == 0) then
go to \3
else
go to \4
end if"""

DEFAULT_RULES = {
    "enddo": "end do",
    "endif": "end if",
    "elseif": "else if",
    r"real\*8": "double precision",
    r"\bcall(\w{4,6}\s*\()": r"call \1",  # Call without space after
    r"go\s*to\s*(\d+)\b": r"go to \1",  # All forms of "go to"
    REGEX_ARITHMETIC_IF: REPLACE_ARITHMETIC_IF,
}


def replace_words(code: list[str],
                  rules: dict[str, str] = DEFAULT_RULES) -> list[str]:
    all_res = compile_all_regex(rules)
    return run_line_filter(
        code,
        partial(replace_words_in_line, all_res=all_res)
    )


def replace_words_in_line(line: str, all_res) -> str:
    for pattern, replace in all_res:
        line = pattern.sub(replace, line)
    return line


def compile_all_regex(
    rules: dict[str, str] = DEFAULT_RULES
) -> list[tuple[re.Pattern, str]]:

    def compile_regex(word):
        regex = rf"\b{word}\b"
        return re.compile(regex, re.IGNORECASE)

    return [
        (compile_regex(original), replaced)
        for original, replaced in rules.items()
    ]
