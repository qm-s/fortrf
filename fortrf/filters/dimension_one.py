from .utils import run_line_filter


def replace_dimension_one_with_star(code: list[str]) -> list[str]:
    return run_line_filter(code, replace_dimension_one_in_line)


def replace_dimension_one_in_line(line: str) -> str:
    if not line.lower().startswith("dimension "):
        return line
    result = line[:10]
    symbol = ""
    level = 0
    for c in line[10:]:
        symbol += c
        if c == "(":
            level += 1
        if c == ")":
            level -= 1
            if level == 0:
                result += replace_dimension_one_in_symbol(symbol)
                symbol = ""
    result += replace_dimension_one_in_symbol(symbol)
    return result


def replace_dimension_one_in_symbol(symbol: str) -> str:
    if len(symbol) < 3 or not symbol.endswith(")"):
        return symbol
    s = symbol.replace(" ", "")
    if s[-2] == "1" and s[-3] in "(,":
        return symbol[:-2] + "*)"
    return symbol
