import re
from typing import Iterator


def locate_subroutine_calls(lines, name) -> Iterator[int]:
    re_subr = re.compile(r"call\s+" + name + r"\b", re.IGNORECASE)
    for lineno, line in enumerate(lines):
        if re_subr.search(line):
            yield lineno
