import itertools

from .f77 import split_comment
from .f90line import F90Line


def strip_literals(line: str) -> str:
    """Strip comments and string literals from line"""
    result = ""
    string_literal = None
    for c in line:
        if string_literal:
            if c == string_literal:
                string_literal = None
        else:
            if c in r"\'\"":
                string_literal = c
            elif c == "!":
                return result
            else:
                result += c
    return result


def f90_lines_from_code_lines(code: list[str]) -> list[F90Line]:
    if not code:
        return []
    result = []
    chunk = [code[0]]
    for cl in code[1:]:
        stripped = cl.lstrip()
        needs_continuation = split_comment(chunk[-1])[0].rstrip().endswith("&")
        if not (needs_continuation or stripped.startswith("&")):
            result.append(F90Line("\n".join(chunk)))
            chunk = []
        chunk.append(cl)
    result.append(F90Line("\n".join(chunk)))
    return result


def code_lines_from_f90_lines(lines: list[F90Line]) -> list[str]:

    def get_code_lines(line: F90Line) -> list[str]:
        code = line.get_code()
        if code is None:
            return []
        elif code == "":
            return [""]
        else:
            return code.splitlines()

    return list(itertools.chain.from_iterable(
        get_code_lines(line) for line in lines
    ))
