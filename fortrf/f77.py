from typing import Iterable


def is_line_cpp(line: str) -> bool:
    return line.startswith("#")


def is_line_comment(line: str) -> bool:
    return line and line[0] in r"cC*dD!"


def is_line_continuation(line: str) -> bool:
    if is_line_cpp(line) or is_line_comment(line):
        return False
    return len(line) > 6 and line[5] not in " 0"


def split_comment(line: str) -> tuple[str, str]:

    def find_outside_string_literal(needle: str, haystack: str) -> int:
        assert len(needle) == 1
        string_literal = None
        for i, c in enumerate(haystack):
            if string_literal:
                if c == string_literal:
                    string_literal = None
            else:
                if c in "\'\"":
                    string_literal = c
                elif c == needle:
                    return i
        return -1

    idx = find_outside_string_literal("!", line)
    return (line, "") if idx == -1 else (line[:idx], line[idx:])


def comment_to_f90(line: str) -> str:
    return "! " + line[1:].lstrip()


def continuation_to_f90(line: str) -> str:
    return "& " + line[6:].lstrip()


def to_f90(lines: Iterable[str]) -> list[str]:

    def add_ampersand(line: str) -> str:
        code, comment = split_comment(line)
        code = code.rstrip() + " &"
        return f"{code}  {comment}" if comment else code

    result = []
    last_code_line = None
    for line in lines:
        if is_line_cpp(line):
            result.append(line)
        elif is_line_comment(line):
            result.append(comment_to_f90(line))
        elif is_line_continuation(line):
            result[last_code_line] = add_ampersand(result[last_code_line])
            result.append(continuation_to_f90(line))
            last_code_line = len(result) - 1
        else:
            result.append(line)
            last_code_line = len(result) - 1
    return result
