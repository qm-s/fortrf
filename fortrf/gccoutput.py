"""Extract info from gcc output"""


import re


def find_undefined_variables(compiler_errors):
    re_var = re.compile(
        r"Error: Symbol ‘(\w+)’ at \(1\) has no IMPLICIT type"
    )
    result = []
    for line in compiler_errors.splitlines():
        if match := re_var.match(line):
            result.append(match.group(1))
    return result
